'use strict';
myApp.controller('itemListController', ['$scope', 'itemResource', 'itemCategoryResource', 'dateTimeService', '$routeParams', '$location',
function ($scope, itemResource, itemCategoryResource, dateTimeService, $routeParams, $location) {
    var getPaginatedItems = function () {
            
        var resource = $routeParams.category == null? itemResource:itemCategoryResource;
        var paginatedVM = resource.getPaginated({
            'category': $routeParams.category,
            'title': $routeParams.title,
            'description': $routeParams.description,
            'page': $routeParams.page,
            'pageSize': $routeParams.pageSize,
            'showFinished': $routeParams.showFinished
        }, function () {
            $scope.category = $routeParams.category;
            $scope.items = paginatedVM.Items;
            $scope.pageLinks = [];
            for (var i = 0; i < paginatedVM.PageCount; i++) {
                if (i != paginatedVM.Page) {
                    $scope.pageLinks.push(i);
                }
            }
            var firstItem = paginatedVM.Page * paginatedVM.PageSize + 1
            var lastItem = firstItem + $scope.items.length - 1;
            $scope.showingDisplay = "Showing " + firstItem + "-" + lastItem + " of " + paginatedVM.TotalItemCount;
        });
    }   

    $scope.gotoPage = function (num) {
        $location.search('page', num);
    }

    $scope.refresh = function () {
        getPaginatedItems();
    }
    getPaginatedItems();
    $scope.displayDate = function (date) {
        return dateTimeService.getLocalEndTimeString(date);
    };
}]);

myApp.controller('itemViewController', ['$scope', '$interval', 'itemResource', 'bidResource', '$routeParams', 'moment', 'errorService', 'dateTimeService',
    function ($scope, $interval, itemResource, bidResource, $routeParams, moment, errorService, dateTimeService) {
        $scope.message = '';
        var itemId = $routeParams.itemId;
        var timer;
        var updateTimeLeft = function (endTime) {
            $scope.timeLeftString = dateTimeService.getTimeLeftString(endTime);
        }

        var getItem = function () {
            if (angular.isDefined(timer)) {
                $interval.cancel(timer);
                timer = undefined;
            }
            $scope.item = itemResource.get({ itemId: itemId },
                function (success) {
                    var endTime = success.EndTime;
                    $scope.endTimeString = dateTimeService.getLocalEndTimeString(endTime);
                    updateTimeLeft(endTime);
                    $scope.minimumBid = success.CurrentSecondPrice + 0.1;
                    if (success.Finished) {
                    }
                    else {
                        timer = $interval(function () {
                            updateTimeLeft(endTime);
                            if ($scope.timeLeftString == "Finished") {
                                getItem();
                                $interval.cancel(timer);
                                timer = undefined;
                            }
                        }, 1000);
                    }
                });
        }

        $scope.refresh = function () {
            getItem();
        }

        $scope.placeBid = function () {
            var bid = {};
            bid.Amount = $scope.bid;
            bid.ItemId = $scope.item.Id;
            bidResource.save(bid,
                function (success) {
                    $scope.bid = '';
                    $scope.message = "Bid placed"
                    getItem();
                },
                function (error) {
                    $scope.message = errorService.getErrorMessages(error);
                });
        }

        getItem();

        $scope.$on('$destroy', function () {
            if (angular.isDefined(timer)) {
                $interval.cancel(timer);
                timer = undefined;
            }
        });
    }]);

myApp.controller('addItemController', ['$scope', 'itemResource', 'categoryResource', '$location', 'errorService',
    function ($scope, itemResource, categoryResource, $location, errorService) {
        $scope.message = '';
        $scope.categories = categoryResource.query(function(){
            $scope.Item = {};
            $scope.Item.Category = $scope.categories[0];       
        });

        $scope.addItem = function () {
            itemResource.save($scope.Item,
                function (success) {
                    $location.path("/items/" + success.Id);
                },
                function (error) {
                    $scope.message = errorService.getErrorMessages(error);
                });
        };
    }]);