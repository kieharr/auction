'use strict';
myApp.controller('homeController', ['$scope', '$location', 'categoryResource', function ($scope, $location, categoryResource) {
    $scope.searchWhat = "title";
    $scope.categories = categoryResource.query();
    $scope.category = "Any";
    $scope.search = function () {
        if ($scope.category == "Any") {
            $location.path('/items').search($scope.searchWhat, $scope.searchString);
        }
        else {
            $location.path('/items').search($scope.searchWhat, $scope.searchString).search("category" ,$scope.category);
        }
        
    }
}]);