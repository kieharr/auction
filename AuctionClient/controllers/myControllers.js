'use strict';
myApp.controller('myBuyingController', ['$scope', 'myItemBuyingResource', 'myItemBuyingCategoryResource', 'dateTimeService', '$routeParams', '$location',
function ($scope, itemBuyingResource, itemBuyingCategoryResource, dateTimeService, $routeParams, $location) {
    var getPaginatedItems = function () {
            
        var resource = $routeParams.category == null? itemBuyingResource: itemBuyingCategoryResource;
        var paginatedVM = resource.getPaginated({
            'category': $routeParams.category,
            'title': $routeParams.title,
            'description': $routeParams.description,
            'page': $routeParams.page,
            'pageSize': $routeParams.pageSize,
            'showFinished': $routeParams.showFinished
        }, function () {
            $scope.category = $routeParams.category;
            $scope.items = paginatedVM.Items;
            $scope.pageLinks = [];
            for (var i = 0; i < paginatedVM.PageCount; i++) {
                if (i != paginatedVM.Page) {
                    $scope.pageLinks.push(i);
                }
            }
            var firstItem = paginatedVM.Page * paginatedVM.PageSize + 1
            var lastItem = firstItem + $scope.items.length - 1;
            $scope.showingDisplay = "Showing " + firstItem + "-" + lastItem + " of " + paginatedVM.TotalItemCount;
        });
    }   

    $scope.gotoPage = function (num) {
        $location.search('page', num);
    }

    $scope.refresh = function () {
        getPaginatedItems();
    }
    getPaginatedItems();
    $scope.displayDate = function (date) {
        return dateTimeService.getLocalEndTimeString(date);
    };
}]);

myApp.controller('mySellingController', ['$scope', 'myItemSellingResource', 'myItemSellingCategoryResource', 'dateTimeService', '$routeParams', '$location',
function ($scope, myItemSellingResource, myItemSellingCategoryResource, dateTimeService, $routeParams, $location) {
    var getPaginatedItems = function () {

        var resource = $routeParams.category == null ? myItemSellingResource : myItemSellingCategoryResource;
        var paginatedVM = resource.getPaginated({
            'category': $routeParams.category,
            'title': $routeParams.title,
            'description': $routeParams.description,
            'page': $routeParams.page,
            'pageSize': $routeParams.pageSize,
            'showFinished': $routeParams.showFinished
        }, function () {
            $scope.category = $routeParams.category;
            $scope.items = paginatedVM.Items;
            $scope.pageLinks = [];
            for (var i = 0; i < paginatedVM.PageCount; i++) {
                if (i != paginatedVM.Page) {
                    $scope.pageLinks.push(i);
                }
            }
            var firstItem = paginatedVM.Page * paginatedVM.PageSize + 1
            var lastItem = firstItem + $scope.items.length - 1;
            $scope.showingDisplay = "Showing " + firstItem + "-" + lastItem + " of " + paginatedVM.TotalItemCount;
        });
    }

    $scope.gotoPage = function (num) {
        $location.search('page', num);
    }

    $scope.refresh = function () {
        getPaginatedItems();
    }
    getPaginatedItems();
    $scope.displayDate = function (date) {
        return dateTimeService.getLocalEndTimeString(date);
    };
}]);
