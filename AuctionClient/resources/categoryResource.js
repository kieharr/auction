﻿'use strict';
myApp.factory('categoryResource', ['$resource', function ($resource) {
    return $resource('http://localhost:13928/api/Categories/');
}]);