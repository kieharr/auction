﻿'use strict';
myApp.factory('itemResource', ['$resource', function ($resource) {
    return $resource('http://localhost:13928/api/Items/:itemId', { itemId: "@id" }, {
        getPaginated: { method: 'GET' },
    });
}]);

myApp.factory('itemCategoryResource', ['$resource', function ($resource) {
    return $resource('http://localhost:13928/api/Items/:category', { category: "@category" }, {
        getPaginated: { method: 'GET' },
    });
}]);


myApp.factory('myItemSellingResource', ['$resource', function ($resource) {
    return $resource('http://localhost:13928/api/My/Items/Selling', { }, {
        getPaginated: { method: 'GET' },
    });
}]);

myApp.factory('myItemSellingCategoryResource', ['$resource', function ($resource) {
    return $resource('http://localhost:13928/api/My/Items/Selling/:category', { category: "@category" }, {
        getPaginated: { method: 'GET' },
    });
}]);

myApp.factory('myItemBuyingResource', ['$resource', function ($resource) {
    return $resource('http://localhost:13928/api/My/Items/Buying', {}, {
        getPaginated: { method: 'GET' },
    });
}]);

myApp.factory('myItemBuyingCategoryResource', ['$resource', function ($resource) {
    return $resource('http://localhost:13928/api/My/Items/Buying/:category', { category: "@category" }, {
        getPaginated: { method: 'GET' },
    });
}]);