﻿'use strict';
myApp.factory('bidResource', ['$resource', function ($resource) {
    return $resource('http://localhost:13928/api/Bids/:bidId', { bidId: "@id" });
}]);