﻿myApp.service('errorService', function () {
    this.getErrorMessages = function (response) {
        var errors = [];

        if (response.data.Message) {
            errors.push(response.data.Message);
        }
        for (var key in response.data.ModelState) {
            for (var i = 0; i < response.data.ModelState[key].length; i++) {
                errors.push(response.data.ModelState[key][i]);
            }
        }
        var message = "Error:" + errors.join(' ');
        return message;
    }
});

myApp.service('dateTimeService', function () {
    var dateFormat = "ddd DD MMM YYYY - HH:mm:ss";

    this.getStringFromDate = function (date) {
        var dateMoment = moment.utc(date);
        return dateMoment.format(dateFormat);
    }

    this.getTimeLeftString = function(endTime){
        var utcTimeNow = moment.utc();
        var utcEndTime = moment.utc(endTime);
        var timeLeftMoment = moment.duration(utcEndTime.diff(utcTimeNow));
        return timeLeftMoment <= 0 ? 'Finished' :
            Math.floor(timeLeftMoment.asDays()) + " days " +
            timeLeftMoment.hours() + " hours " +
            timeLeftMoment.minutes() + " minutes " +
            timeLeftMoment.seconds() + " seconds";
    }

    this.getLocalEndTimeString = function (endTime) {
        var utcEndTime = moment.utc(endTime);
        var localEndTime = moment.utc(endTime).local();
        var utcTimeNow = moment.utc();
        var timeLeftMoment = moment.duration(utcEndTime.diff(utcTimeNow));
        return timeLeftMoment <= 0 ? 'Finished' : localEndTime.format(dateFormat);
    }
});