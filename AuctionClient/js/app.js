var myApp = angular.module('myApp', [
	'ngRoute',
    'LocalStorageModule',
    'ngResource'
]);

myApp.constant("moment", moment);

myApp.config(['$routeProvider', function($routeProvider){
	
	$routeProvider.when('/home', {
		controller: 'homeController',
		templateUrl: '/views/home.html'
	}).
        when("/home/:id", {
	    controller: "loginController",
	    templateUrl: "/views/home.html"
	}).
    when("/login", {
        controller: "loginController",
        templateUrl: "/views/auth/login.html"
    }).
 
    when("/signup", {
        controller: "signupController",
        templateUrl: "/views/auth/signup.html"
    }).
    when("/my/items/selling", {
        controller: "mySellingController",
        templateUrl: "/views/itemList.html"
    }).
    when("/my/items/buying", {
        controller: "myBuyingController",
        templateUrl: "/views/itemList.html"
    }).
    when("/items", {
        controller: "itemListController",
        templateUrl: "/views/itemList.html"
    }).
    when("/items/:itemId", {
        controller: "itemViewController",
        templateUrl: "/views/itemView.html"
    }).
    when("/add/item", {
        controller: "addItemController",
        templateUrl: "/views/addItem.html"
    }).
	otherwise({
		redirectTo: '/home'
	});
}]);

myApp.run(['authService', function (authService) {
    authService.fillAuthData();
}]);

myApp.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});