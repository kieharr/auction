﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AuctionServer.Models;
using System.Security.Claims;
using AuctionServer.ViewModels;
using AutoMapper;
using System.Globalization;

namespace AuctionServer.Controllers
{
    [Authorize]
    public class ItemsController : BaseController
    {

        private IQueryable<Item> GetItemQueryable(string category = null, bool showFinished = false, bool my = false, bool selling = true)
        {
            var queryString = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);

            IQueryable<Item> itemQuery = db.Items;
            var user = GetCurrentUser();
            if (my)
            {
                if (selling)
                {
                    itemQuery = itemQuery.Where(i => i.Owner.UserName.Equals(user.UserName));
                }
                else
                {
                    itemQuery = itemQuery.Where(i => i.Bids.Any(j => j.Bidder.UserName == user.UserName));
                }
            }
            else
            {
                itemQuery = itemQuery.Where(i => !i.Owner.UserName.Equals(user.UserName));
            }

            if (!showFinished)
            {
                itemQuery = itemQuery.Where(i => i.EndTime >= DateTime.UtcNow);
            }
            if (category != null)
            {
                itemQuery = itemQuery.Where(i => i.Category.Name == category);
            }
            if (queryString.ContainsKey("title"))
            {
                var value = queryString["title"];
                itemQuery = itemQuery.Where(i => i.Title.Contains(value));
            }
            if (queryString.ContainsKey("description"))
            {
                var value = queryString["description"];
                itemQuery = itemQuery.Where(i => i.Description.Contains(value));
            }
            return itemQuery;
        }

        private PaginatedItemViewModel GetPaginatedViewModel(IQueryable<Item> itemQuery, int page, int pageSize)
        {
            var totalItemCount = itemQuery.Count();
            var pageCount = (int)Math.Ceiling((double)totalItemCount / pageSize);

            var itemResults = itemQuery.OrderByDescending(i => i.EndTime).Skip(pageSize * page).Take(pageSize);

            IList<ItemViewModel> itemVMs = new List<ItemViewModel>();
            foreach (Item item in itemResults)
            {
                var itemVM = Mapper.Map<ItemViewModel>(item);
                itemVMs.Add(itemVM);
            }
            PaginatedItemViewModel paginatedVM = new PaginatedItemViewModel
            {
                Items = itemVMs,
                TotalItemCount = totalItemCount,
                PageCount = pageCount,
                Page = page,
                PageSize =pageSize
            };
            return paginatedVM;
        }

        [HttpGet]
        [Route("api/My/Items/{buySell}/{category?}")]
        public PaginatedItemViewModel GetMySellingItems(string buySell, string category = null, bool showFinished = true, int page = 0, int pageSize = 10)
        {
            bool selling = buySell.ToLower().Equals("selling");

            IQueryable<Item> itemQuery = GetItemQueryable(category, showFinished, true, selling);

            PaginatedItemViewModel paginatedVM = GetPaginatedViewModel(itemQuery, page, pageSize);

            return paginatedVM;
        }

        // GET: api/Items
        [HttpGet]
        [Route("api/Items/{category?}")]
        public PaginatedItemViewModel GetItems(string category = null, bool showFinished = false, int page = 0, int pageSize = 10)
        {
            IQueryable<Item> itemQuery = GetItemQueryable(category, showFinished);

            PaginatedItemViewModel paginatedVM = GetPaginatedViewModel(itemQuery, page, pageSize);

            return paginatedVM;
        }

        // GET: api/Items/5
        [HttpGet]
        [Route("api/Items/{id:guid}")]
        public IHttpActionResult GetItem(Guid id)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            var itemVM = Mapper.Map<ItemViewModel>(item);
            if(item.Owner == GetCurrentUser())
            {
                itemVM.OwnItem = true;
            }
            return Ok(itemVM);
        }

        [HttpPost]
        [Route("api/Items", Name = "CreateItem")]
        // POST: api/Items
        public IHttpActionResult PostItem(CreateItemViewModel createItemVM)
        {
            if(createItemVM == null)
            {
                return BadRequest("No model passed");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var currentUser = GetCurrentUser();
            var category = db.Categories.Where(i => i.Name == createItemVM.Category).FirstOrDefault();
            if(category == null)
            {
                return BadRequest("Invalid Category");
            }
            var endTime = DateTime.UtcNow.AddHours(createItemVM.AuctionLengthInHours);
            Item item = new Item { Category = category ,Description = createItemVM.Description, StartingPrice = createItemVM.StartingPrice, Title = createItemVM.Title, Owner = currentUser, EndTime = endTime};

            db.Items.Add(item);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ItemExists(item.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            //var itemVM = Mapper.Map<ItemViewModel>(item);
            return CreatedAtRoute("CreateItem", new { id = item.Id }, item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ItemExists(Guid id)
        {
            return db.Items.Count(e => e.Id == id) > 0;
        }
    }
}