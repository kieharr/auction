﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AuctionServer.Controllers
{
    public class CategoriesController : BaseController
    {
        [Route("api/Categories/")]
        public IQueryable<string> GetCategories()
        {
            return db.Categories.Select(i => i.Name);
        }
    }
}
