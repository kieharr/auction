﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AuctionServer.Models;
using System.Security.Claims;
using AuctionServer.ViewModels;

namespace AuctionServer.Controllers
{
    [Authorize]
    public class BidsController : BaseController
    {
        // POST: api/Bids
        [HttpPost]
        [Route("api/Bids", Name = "CreateBid")]
        public IHttpActionResult PostBid(CreateBidViewModel bidVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var item = db.Items.Find(bidVM.ItemId);
            if(item == null)
            {
                return BadRequest("Item not found.");
            }
            if (item.Finished)
            {
                return BadRequest("Bidding is closed for this auction");
            }
            var bidder = GetCurrentUser();
            if (item.Owner == bidder)
            {
                return BadRequest("You can't bid on your own auction");
            }
            var minBid = item.GetCurrentSecondPrice() + Bid.MINUMUM_INCREMENT;
            if(bidVM.Amount < minBid)
            {
                return BadRequest(String.Format("Bid amount must be higher than {0}", minBid.ToString("C")));
            }
            var bid = new Bid { Amount = bidVM.Amount, Item = item, Bidder=bidder, TimeStamp = DateTime.UtcNow};

            db.Bids.Add(bid);
            db.SaveChanges();

            return CreatedAtRoute("CreateBid", new { id = bid.Id }, bidVM);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BidExists(Guid id)
        {
            return db.Bids.Count(e => e.Id == id) > 0;
        }
    }
}