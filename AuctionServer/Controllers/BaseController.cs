﻿using AuctionServer.DAL;
using AuctionServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace AuctionServer.Controllers
{
    public abstract class BaseController : ApiController
    {
        protected ApplicationDbContext db = new ApplicationDbContext();
        protected ApplicationUser GetCurrentUser()
        {
            var identity = User.Identity as ClaimsIdentity;
            Claim identityClaim = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            return db.Users.FirstOrDefault(u => u.Id == identityClaim.Value);
        }
    }
}