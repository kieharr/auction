﻿using AuctionServer.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionServer.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<AuctionServer.Models.Item> Items { get; set; }

        public System.Data.Entity.DbSet<AuctionServer.Models.Bid> Bids { get; set; }
        public System.Data.Entity.DbSet<AuctionServer.Models.Category> Categories { get; set; }
    }
}