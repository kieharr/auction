﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AuctionServer.ViewModels
{
    public class CreateBidViewModel
    {
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public Guid ItemId { get; set; }
    }
}