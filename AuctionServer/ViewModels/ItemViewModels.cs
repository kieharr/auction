﻿using AuctionServer.Models;
using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AuctionServer.ViewModels
{

    public class CreateItemViewModel
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Category { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int AuctionLengthInHours { get; set; }
        [Required]
        [Min(0)]
        public decimal StartingPrice { get; set; }
    }

    public class ItemViewModel
    {
        public Guid Id { get; set; }
        public string OwnerUserName { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime EndTime { get; set; }
        public decimal StartingPrice { get; set; }
        public virtual int NumberOfBids { get; set; }
        public virtual decimal CurrentSecondPrice { get; set; }
        public virtual string Winner {get ;set;}
        public virtual bool Finished { get; set; }
        public bool OwnItem = false;
    }

    public class PaginatedItemViewModel
    {
        public IList<ItemViewModel> Items { get; set; }
        public int TotalItemCount { get; set; }
        public int PageCount { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}