﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using MoreLinq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace AuctionServer.Models
{
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [JsonIgnore]
        [Required]
        public virtual ApplicationUser Owner { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public virtual Category Category { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime EndTime { get; set; }
        [Required]
        public decimal StartingPrice { get; set; }
        [JsonIgnore]
        public virtual ICollection<Bid> Bids { get; set; }
        public virtual int NumberOfBids
        {
            get { return Bids==null? 0:Bids.Count; }
        }

        //Current price for Second Price auction
        public decimal GetCurrentSecondPrice()
        {
            var distinctUserBids = Bids.DistinctBy(i => i.Bidder).ToList();
            return distinctUserBids.Count > 1 ? Bids.Select(i => i.Amount).OrderByDescending(i => i).Skip(1).First(): StartingPrice;
        }
        public string GetWinner()
        {
            return Bids.Count > 0 ? Bids.MaxBy(i => i.Amount).Bidder.UserName : "N/A";
        }
        public bool Finished
        {
            get { return DateTime.UtcNow >= EndTime; }
        }
    }
}