﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using AuctionServer.Models;
using AuctionServer.ViewModels;

namespace AuctionServer.App_Start
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<Item, ItemViewModel>();
        }
    }
}