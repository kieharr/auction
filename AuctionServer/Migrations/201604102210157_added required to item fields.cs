namespace AuctionServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedrequiredtoitemfields : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Items", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.Items", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Items", new[] { "Category_Id" });
            DropIndex("dbo.Items", new[] { "Owner_Id" });
            AlterColumn("dbo.Items", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Items", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.Items", "Category_Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Items", "Owner_Id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Items", "Category_Id");
            CreateIndex("dbo.Items", "Owner_Id");
            AddForeignKey("dbo.Items", "Category_Id", "dbo.Categories", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Items", "Owner_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Items", "Owner_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Items", "Category_Id", "dbo.Categories");
            DropIndex("dbo.Items", new[] { "Owner_Id" });
            DropIndex("dbo.Items", new[] { "Category_Id" });
            AlterColumn("dbo.Items", "Owner_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.Items", "Category_Id", c => c.Guid());
            AlterColumn("dbo.Items", "Description", c => c.String());
            AlterColumn("dbo.Items", "Title", c => c.String());
            CreateIndex("dbo.Items", "Owner_Id");
            CreateIndex("dbo.Items", "Category_Id");
            AddForeignKey("dbo.Items", "Owner_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Items", "Category_Id", "dbo.Categories", "Id");
        }
    }
}
