namespace AuctionServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class autogenerateguidforitem : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Items");
            AlterColumn("dbo.Items", "Id", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newsequentialid()"));
            AddPrimaryKey("dbo.Items", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Items");
            AlterColumn("dbo.Items", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Items", "Id");
        }
    }
}
