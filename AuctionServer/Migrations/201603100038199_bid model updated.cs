namespace AuctionServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bidmodelupdated : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bids",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        ItemId = c.Guid(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TimeStamp = c.DateTime(nullable: false),
                        Bidder_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Bidder_Id)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.Bidder_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bids", "ItemId", "dbo.Items");
            DropForeignKey("dbo.Bids", "Bidder_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Bids", new[] { "Bidder_Id" });
            DropIndex("dbo.Bids", new[] { "ItemId" });
            DropTable("dbo.Bids");
        }
    }
}
