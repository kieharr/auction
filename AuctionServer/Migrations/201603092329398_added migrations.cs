namespace AuctionServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedmigrations : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        EndTime = c.DateTime(nullable: false),
                        StartingPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Owner_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Owner_Id)
                .Index(t => t.Owner_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Items", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Items", new[] { "Owner_Id" });
            DropTable("dbo.Items");
        }
    }
}
