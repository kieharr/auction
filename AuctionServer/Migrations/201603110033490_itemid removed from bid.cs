namespace AuctionServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemidremovedfrombid : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Bids", "ItemId", "dbo.Items");
            DropIndex("dbo.Bids", new[] { "ItemId" });
            RenameColumn(table: "dbo.Bids", name: "ItemId", newName: "Item_Id");
            AlterColumn("dbo.Bids", "Item_Id", c => c.Guid());
            CreateIndex("dbo.Bids", "Item_Id");
            AddForeignKey("dbo.Bids", "Item_Id", "dbo.Items", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bids", "Item_Id", "dbo.Items");
            DropIndex("dbo.Bids", new[] { "Item_Id" });
            AlterColumn("dbo.Bids", "Item_Id", c => c.Guid(nullable: false));
            RenameColumn(table: "dbo.Bids", name: "Item_Id", newName: "ItemId");
            CreateIndex("dbo.Bids", "ItemId");
            AddForeignKey("dbo.Bids", "ItemId", "dbo.Items", "Id", cascadeDelete: true);
        }
    }
}
