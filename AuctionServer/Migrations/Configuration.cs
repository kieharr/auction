namespace AuctionServer.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AuctionServer.DAL.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "AuctionServer.Models.ApplicationDbContext";
        }

        protected override void Seed(AuctionServer.DAL.ApplicationDbContext context)
        {
            context.Categories.AddOrUpdate(
                i => i.Name,
                new Category { Name = "Automobiles", Id = Guid.NewGuid() },
                new Category { Name = "Music", Id = Guid.NewGuid() },
                new Category { Name = "Books", Id = Guid.NewGuid() }
            );


            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
